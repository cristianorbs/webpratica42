/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package inscricao.faces.mngbeans;

import inscricao.entity.Candidato;
import java.util.ArrayList;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import utfpr.faces.support.PageBean;

/**
 *
 * @author cristianopc
 */
@ManagedBean(name="registroBean")
@ApplicationScoped
public class RegistroBean extends PageBean{
    private ArrayList<Candidato> candidateList = new ArrayList<>();

    public ArrayList<Candidato> getCandidateList() {
        return candidateList;
    }

    public void setCandidateList(ArrayList<Candidato> candidateList) {
        this.candidateList = candidateList;
    }
    
    public void addCandidate(Candidato c){
        candidateList.add(c);
    }
}
